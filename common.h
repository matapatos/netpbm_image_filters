#ifndef _COMMON_H
#define _COMMON_H

#define ERR_ARGS 1
#define ERR_IO 2
#define ERR_ALLOC 3
#define ERR_FORMAT 4
#define ERR_UNKNOWN 5

#include <sys/time.h>

extern int running;

typedef enum {P1=1, P2=2, P3=3, P4=4, P5=5, P6=6} FORMAT_T;

typedef struct {
	// green and blue components will not be used for bit and grayscale formats
	unsigned short rgb[3];
} POINT_T;

typedef struct {
	FORMAT_T format;
	int lines;
	int columns;
	int max_color;
	int is_16bit;
	POINT_T **points;
} NET_PBM;

typedef struct {
	unsigned long pixel_count;
	unsigned long line_count;
} STATS_T;

int pbm_parse_file(const char *filename, NET_PBM *pbm, STATS_T *stats);
int pbm_write_file(const char *filename,  NET_PBM *pbm);
void pbm_free(NET_PBM *pbm);
double elapsed(struct timeval *end, struct timeval *start);

void install_signal_handler(void);

char *create_output_filename(const char *input_filename, const char *prefix, const char *ext);

const char *get_extension(FORMAT_T);
#endif
