/**
* @file main.c
* @brief Description
* @date 01-01-2006
* @author eiYxxxxx@student.estg.ipleiria.pt
*/

#include <stdio.h>
#include <time.h>

#include "debug.h"
#include "memory.h"
#include "config.h"

#include "common.h"
#include "etapa1.h"
#include "etapa2.h"

#define ERR_MEDIAN_FILTER 6

enum median_filter_size{size_5 = 5, size_9 = 9, size_25 = 25, size_49 = 49};


int main(int argc, char *argv[]) {
	struct gengetopt_args_info args;

	if (cmdline_parser(argc, argv, &args) != 0)
		exit(ERR_ARGS);

	STATS_T stats;
	stats.line_count = 0;
	stats.pixel_count = 0;

	char *output_file = args.output_given ? args.output_arg : NULL;

	struct timeval start;
	if (gettimeofday(&start, NULL) != 0)
		ERROR(ERR_UNKNOWN, "gettimeofday failed");

	int status = 0;
	install_signal_handler();
	if (args.to_pbm_given) {
		if (args.threshold_arg < 0 || args.threshold_arg > 1) {
			printf("Invalid threshold value\n");
			cmdline_parser_free(&args);
			exit(ERR_ARGS);
		}
		status = to_pbm(args.to_pbm_arg, args.threshold_arg, output_file, &stats);
	} else if (args.to_pgm_given) {
		status = to_pgm(args.to_pgm_arg, output_file, &stats);
	} else if (args.to_ASCII_given) {
		status = to_ascii(args.to_ASCII_arg, output_file, &stats);
	} else if (args.to_binary_given) {
		status = to_binary(args.to_binary_arg, output_file, &stats);
	} else if (args.median_filter_given) {
		//filter-size options: "5, 9, 25, 49"
		switch(args.median_filter_size_arg){
			case size_5: status = to_median_filter(args.median_filter_arg, output_file, &stats, args.median_filter_size_orig, size_5, args.median_max_threads_arg, args.median_num_blocks_arg);
			break;

			case size_9: status = to_median_filter(args.median_filter_arg, output_file, &stats, args.median_filter_size_orig, size_9, args.median_max_threads_arg, args.median_num_blocks_arg);
			break;

			case size_25: status = to_median_filter(args.median_filter_arg, output_file, &stats, args.median_filter_size_orig, size_25, args.median_max_threads_arg, args.median_num_blocks_arg);
			break;

			case size_49: status = to_median_filter(args.median_filter_arg, output_file, &stats, args.median_filter_size_orig, size_49, args.median_max_threads_arg, args.median_num_blocks_arg);
			break;

			default: printf("./pbmTool: invalid argument, \"%s\", for option `--median-filter-size'\n", args.median_filter_size_orig);
					status = ERR_MEDIAN_FILTER;
			break;
		}
		
	} else if (args.sobel_filter_given) {
		status = to_sobel_filter(args.sobel_filter_arg, output_file, &stats, args.sobel_max_threads_arg, args.sobel_num_blocks_arg);
	} else if (args.logic_operation_given) {
		status = logic_operation(args.logic_operation_arg, output_file, &stats, args.logic_max_threads_arg, args.logic_num_blocks_arg, args.operation_arg);
	} else {
		printf("Must specify at least one operation\n");
		cmdline_parser_free(&args);
		exit(ERR_ARGS);
	}
	struct timeval end;
	if (gettimeofday(&end, NULL) != 0)
		ERROR(ERR_UNKNOWN, "gettimeofday failed");

	if (running == 0) {
		time_t t;
		struct tm now;

		time(&t);
		localtime_r(&t, &now);
		fprintf(stderr, "\nOperation interrupted by user @%d-%02d-%02d %02dh%02d\n",
			now.tm_year + 1900, now.tm_mon + 1, now.tm_mday,
			now.tm_hour, now.tm_min);
	} else if (status == 0) {
		fprintf(stderr, "# %s\n", argv[0]);
		fprintf(stderr, "# options:");
		int i;
		for (i = 1; i < argc; i++)
			fprintf(stderr, " %s", argv[i]);

		fprintf(stderr, "\n# lines: %li\n", stats.line_count);
		fprintf(stderr, "# pixels: %li\n", stats.pixel_count);
		fprintf(stderr, "# execution time: %.2lfms\n", elapsed(&end, &start));
	} else {
		printf("Ops. Operation failed with status %d\n", status);
	}

	cmdline_parser_free(&args);

	return 0;
}
