#ifndef _ETAPA2_H_

#define _ETAPA2_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <errno.h>
#include "common.h"
#include "debug.h"
#include "memory.h"

/*------MACROS--------*/

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) < (Y) ? (Y) : (X))
#define IS_ODD(X) (X%2==1 ? 1 : 0) /*verify if X is a odd number*/
#define AVERAGE(X,Y) ((X+Y)/2)

//ERROR DECLARATIONs
#define ERR_PTHREAD_CREATE 1
#define ERR_PTHREAD_JOIN 2
#define ERR_MUTEX_DESTROY 3
#define ERR_COND_DESTROY 4
#define ERR_MUTEX_LOCK 5
#define ERR_MUTEX_UNLOCK 6
#define ERR_COND_WAIT 7
#define ERR_COND_BROAD 8
#define ERR_COND_SIGNAL 9
#define ERR_PTHREAD_CANCEL 10

typedef void(*filter_func_t)(void *arg, int line_index, int line_blocks);
typedef int(*operation_func_t)(int first_arg, int sec_arg);
typedef enum{TRUE = 1, FALSE = 0}boolean;

typedef struct{

	int pos_read;
	int pos_write;
	int *buffer;
	boolean stop;
	int total;
	int max_threads;
	int line_blocks;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	void *filter_arg;
	filter_func_t filter;

}PARAM_T;

//STRUCTURE USED BY MEDIAN FILTER
typedef struct{
	
	int filter_size;	
	int sqrt_filter_size;
	//ALTERAR DEPOIS PARA APENAS UMA ESTRUTURA E ATRIBUI SE NO FIM A ESTRUTURA PBM PARA ESCREVER NO FICHEIRO		
	NET_PBM *pbm;
	NET_PBM *matrix;

}TASK_T;

typedef struct{

	int operation;
	operation_func_t func;
	NET_PBM *image1;
	NET_PBM *image2;
	NET_PBM *final_image;

}LOGIC_T;

void initialize_TASK_T(TASK_T *t, NET_PBM *pbm);
void initialize_LOGIC_T(LOGIC_T *logic, NET_PBM *image1, NET_PBM *image2, int operation, int lines, int columns);
void initialize_PARAM_T(PARAM_T *p, int max_threads, int line_blocks);
void free_PARAM_T(PARAM_T *t);
void convertPPMtoPGM(NET_PBM *pbm);
void **get_matrix(int lines, int cols, size_t type);
void producer(PARAM_T *t, int lines);
void *consumer(void *arg);
void main_function(filter_func_t filter, void *arg, int max_threads, int num_blocks, int line_blocks, int lines);

//	MEDIAN FILTER FUNCTIONS-----
int to_median_filter(const char *input_filename, char *output_filename, STATS_T *stats, char *median_filter_size_orig, int median_filter_size, int median_max_threads, int median_num_blocks);
void add_pixel_median(void *t, int line_index, int line_blocks);
unsigned short *add_blocks_median(NET_PBM *pbm, unsigned short *vetor, int line_index, int column_index, int rgb, int *n_elements, int filter_size, int sqrt_filter_size);

//	SOBEL FUNCTIONS
int to_sobel_filter(const char *input_filename, char *output_filename, STATS_T *stats, int sobel_max_threads, int sobel_num_blocks);
void add_pixel_sobel(void *t, int line_index, int line_blocks);
unsigned short **add_blocks_sobel(TASK_T *t, unsigned short **vetor,int line_index, int column_index);

//LOGIC FUNCTIONS
int logic_operation(char **input_files, char *output_filename, STATS_T *stats, int logic_max_threads, int logic_num_blocks, int logic_operation);
char *output_file_logic(char **input_files, char *output_filename, int logic_operation, FORMAT_T format);
void main_function_logic(LOGIC_T *t, NET_PBM *image1, NET_PBM *image2, int logic_max_threads, int logic_num_blocks, int logic_operation);
void add_pixel_logic(void *arg, int line_index, int line_blocks);
int logic_AND_operation(int first_arg, int sec_arg);
int logic_XOR_operation(int first_arg, int sec_arg);
int logic_NOR_operation(int first_arg, int sec_arg);
int logic_OR_operation(int first_arg, int sec_arg);


//UTILITIES QSORT FUNCTION
int comparator (const void * a, const void * b);

#endif
