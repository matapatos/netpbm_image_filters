#ifndef _ETAPA1_H_
#define _ETAPA1_H_

#include "common.h"

int to_pbm(char *input_file, float threshold, char *output_file, STATS_T *stats);
int to_pgm(char *input_file, char *output_file, STATS_T *stats);
int to_ascii(char *input_file, char *output_file, STATS_T *stats);
int to_binary(char *input_file, char *output_file, STATS_T *stats);

#endif
