# netpbm_image_filters

This project contains the C implementation of a multi-threaded application called pbmTool, that is able to apply different types of image filters, like: 

- Contour extraction;
- Noise reduction;
- Logic operations (e.g. AND, OR, XOR and (NOT a) OR b);
- Conversion between file types (e.g. PBM, PGM and PPM).

## Contour extraction ##

```
./pbmTool --sobel-filter="[IMAGE PATH - PBM, PGM or PPM]" --sobel-num-blocks [INT NUMBER] --sobel-max-threads [INT NUMBER]
```

![Sobel filtering](images/operation_example_images/sobel_filtering.png)

Image taken from: *"Computação Gráfica", Volume 2, Edição 1, A. Conci, E. Azevedo, F. R. Leta, ELSEVIER– Capítulo 5*

## Noise reduction ##

```
./pbmTool --median-filter="[IMAGE PATH - PBM, PGM or PPM]" --median-num-blocks [INT NUMBER] --median-max-threads [INT NUMBER] --median-filter-size [INT NUMBER]
```

![Mean filtering](images/operation_example_images/mean_filtering.png)

Image taken from: *"Computação Gráfica", Volume 2, Edição 1, A. Conci, E. Azevedo, F. R. Leta, ELSEVIER– Capítulo 5*

## Logic operations ##

There are four possible logic operations: AND, OR, XOR and NOR.

```
./pbmTool --logic-operation="[IMAGE PATH - PBM, PGM or PPM]" --logic-num-blocks [INT NUMBER] --logic-max-threads [INT NUMBER] --operation [AND, OR, XOR or NOR] --output [FILEPATH FOR THE GENERATED IMAGE]
```

Example of two given pictures:

![Images given](images/operation_example_images/operation_images.png)

Example of the resultant image depending on the operation applied:

![Possible operations](images/operation_example_images/possible_operations.png)
