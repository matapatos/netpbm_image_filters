#include "etapa2.h"

		//filter = NAME OF THE FUNCTION  arg = TASK_T or LOGIC_T
void main_function(filter_func_t filter, void *arg, int max_threads, int num_blocks, int line_blocks, int lines){
		
		//---------INITIALIZE PARAM_T----------
		PARAM_T t;
		t.filter = filter;
		t.filter_arg = arg;
		//VERIFY IF NUMBER BLOCKS ARE LESS THAN NUMBER OF THREADS!
		max_threads = MIN(max_threads, num_blocks);
		initialize_PARAM_T(&t, max_threads, line_blocks);	

		int i;	
		pthread_t tids[max_threads];

		//CREATE THREADS
		for(i=0; i<max_threads; i++){
			if((errno=pthread_create(&tids[i], NULL, consumer, &t))!=0) ERROR(ERR_PTHREAD_CREATE, "pthread_create failed!\n");	
		}

		producer(&t, lines);
		
		// //WAIT FOR ALL THREADS
		for(i=0; i<max_threads; i++){
			if((errno=pthread_join(tids[i], NULL))!=0) ERROR(ERR_PTHREAD_JOIN, "pthread_join failed!\n");
		}

		//DO ALL FREE MEMORY AND DESTROY MUTEX AND CONDITION VARIABLES
		free_PARAM_T(&t);

}

void free_PARAM_T(PARAM_T *t){
		
		free(t->buffer);	

		if((errno=pthread_mutex_destroy(&(t->mutex)))!=0) ERROR(ERR_MUTEX_DESTROY, "pthread_mutex_destroy failed!\n");

		if((errno=pthread_cond_destroy(&(t->cond)))!=0) ERROR(ERR_COND_DESTROY, "pthread_cond_destroy failed!\n");
}



int to_median_filter(const char *input_filename, char *output_filename, STATS_T *stats,  char *median_filter_size_orig, int median_filter_size, int median_max_threads, int median_num_blocks){

	NET_PBM pbm;

	int num;
	if ((num=pbm_parse_file(input_filename, &pbm, stats) != 0) || (num==0 && (pbm.format==1 || pbm.format==4))) {
			printf("Invalid file format\n");
			pbm_free(&pbm);
		return -1;
	}

//INITIALIZE DIFERENTS ITEMS
	TASK_T task;
	task.filter_size = median_filter_size;
	task.sqrt_filter_size = (int) (sqrt((int) median_filter_size)/2);
	//INITIALIZE MATRIXs
	initialize_TASK_T(&task, &pbm);

	int line_blocks = ceil(pbm.lines/median_num_blocks);

	main_function(add_pixel_median, &task, median_max_threads, median_num_blocks, line_blocks, pbm.lines);
		

	if(output_filename==NULL){
		char prefix[strlen("_med_") + strlen(median_filter_size_orig) +1];
		sprintf(prefix, "_med_%s", median_filter_size_orig);
		output_filename = create_output_filename(input_filename, prefix, get_extension(pbm.format));
	}

	if (pbm_write_file(output_filename, task.matrix) != 0)
	printf("Failed to write file %s\n", output_filename);

		//FREEs
		pbm_free(task.matrix);
		free(task.matrix);
		pbm_free(&pbm);
		free(output_filename);

	return 0;
}



int to_sobel_filter(const char *input_filename, char *output_filename, STATS_T *stats, int sobel_max_threads, int sobel_num_blocks){
	
	NET_PBM pbm;


	int num;
	if ((num=pbm_parse_file(input_filename, &pbm, stats) != 0) || (num==0 && (pbm.format==1 || pbm.format==4))) {
			printf("Invalid file format\n");
			pbm_free(&pbm);
		return -1;
	}

	if(pbm.format==3 || pbm.format==6)
		convertPPMtoPGM(&pbm);


	TASK_T task;
	initialize_TASK_T(&task, &pbm);

	int line_blocks = ceil(pbm.lines/sobel_num_blocks);

	main_function(add_pixel_sobel, &task, sobel_max_threads, sobel_num_blocks, line_blocks, pbm.lines);

	if(output_filename==NULL)
		output_filename = create_output_filename(input_filename, "_sob", get_extension(pbm.format));

	if (pbm_write_file(output_filename, task.matrix) != 0)
		printf("Failed to write file %s\n", output_filename);
		


	//FREEs
		pbm_free(task.matrix);
		free(task.matrix);
		pbm_free(&pbm);
		free(output_filename);

	return 0;
}

int logic_operation(char **input_files, char *output_filename, STATS_T *stats, int logic_max_threads, int logic_num_blocks, int logic_operation){
//VALUES logic_operation: AND = 0, OR=1, XOR=2, NOR=3

	//SAVE IN THE STRUCTURE THE FIRST IMAGE
	NET_PBM image1;
	int num;
	if ((num=pbm_parse_file(input_files[0], &image1, stats) != 0) || (num==0 && image1.format!=1 && image1.format!=4)) {
			printf("Invalid file format\n");
			pbm_free(&image1);
		return -1;
	}

	//SAVE THE SECOND IMAGE IN THE STRUCTURE
	NET_PBM image2;
	if ((num=pbm_parse_file(input_files[1], &image2, stats) != 0) || (num==0 && image2.format!=1 && image2.format!=4)) {
			printf("Invalid file format\n");			
			pbm_free(&image1);
			pbm_free(&image2);
		return -1;
	}	

	LOGIC_T logic;
	//NUMBER OF LINES
	int lines = MAX(image1.lines, image2.lines);
	int columns = MAX(image1.columns, image2.columns);

	initialize_LOGIC_T(&logic, &image1, &image2, logic_operation, lines, columns);

	int line_blocks = ceil(lines/logic_num_blocks);

	switch(logic_operation){
		case 0:	logic.func = logic_AND_operation;
		break;
		case 1:	logic.func = logic_OR_operation;
		break;
		case 2: logic.func = logic_XOR_operation;
		break;
		case 3: logic.func = logic_NOR_operation;
		break;
	}

	main_function(add_pixel_logic, &logic, logic_max_threads, logic_num_blocks, line_blocks, lines);



	if(output_filename==NULL)
		output_filename = output_file_logic(input_files, output_filename, logic_operation, image1.format);

	if (pbm_write_file(output_filename, logic.final_image) != 0)
		printf("Failed to write file %s\n", output_filename);

	//FREEs
	free(output_filename);
	pbm_free(&image1);
	pbm_free(&image2);
	pbm_free(logic.final_image);
	free(logic.final_image);

	return 0;
}

void add_pixel_logic(void *arg, int line_index, int line_blocks){
		//VALUES logic_operation: AND = 0, OR=1, XOR=2, NOR=3

		LOGIC_T *logic = arg;
		NET_PBM *img1 = logic->image1;
		NET_PBM *img2 = logic->image2;

		int lines = logic->final_image->lines, columns = logic->final_image->columns, min_lines = MIN(img1->lines, img2->lines), min_cols = MIN(img1->columns, img2->columns), i;
		register int j;

		//AND OPERATION
		for(i=line_index; i<(line_index + line_blocks) && i<lines; i++){
			//VERIFY HAD REACHED MAX LINES OF THE IMAGE WITH LESS LINES
			if(i<min_lines){
				for(j=0; j < columns; j++){
					//VERIFY IF HAD REACHED THE MAX COLUMNS OF THE MIN IMAGE
					if(j<min_cols)
						logic->final_image->points[i][j].rgb[0] = logic->func(img1->points[i][j].rgb[0], img2->points[i][j].rgb[0]);
					//IF(j>min_cols)
					else{
						logic->final_image->points[i][j].rgb[0] = 0;
					}
				}
			}
			else
				for(j=0; j<columns;j++){
					logic->final_image->points[i][j].rgb[0]=0;
				}
		}

}

int logic_NOR_operation(int first_arg, int sec_arg){

	if(sec_arg==1 && first_arg == 0)
		return 0;

	else return 1;
}

int logic_XOR_operation(int first_arg, int sec_arg){
	
	if(first_arg==1)
		if(sec_arg==1)
			return 0;
		else return 1;

	else 
		if(sec_arg==1) return 1;

		else return 0;
}

int logic_OR_operation(int first_arg, int sec_arg){

	if(first_arg==1 || sec_arg==1)
		return 1;

	else return 0;
}

int logic_AND_operation(int first_arg, int sec_arg){

	if(first_arg==1 && sec_arg==1)
		return 1;

	else return 0;
}



void initialize_LOGIC_T(LOGIC_T *logic, NET_PBM *image1, NET_PBM *image2, int operation, int lines, int columns){

	logic->operation = operation;
	logic->image1 = image1;
	logic->image2 = image2;
	logic->final_image = MALLOC(sizeof(NET_PBM));
	*(logic->final_image) = *image1;
	logic->final_image->lines = lines;
	logic->final_image->columns = columns;
	logic->final_image->points = (POINT_T **)get_matrix(lines, columns, sizeof(POINT_T));	

}

char *output_file_logic(char **input_files, char *output_filename, int logic_operation, FORMAT_T format){

		//REMOVE .pbm ON THE STRING
		int dot = '.';
		char *input = input_files[1];
		char *second_file_name = strrchr(input, dot);
		if(second_file_name!=NULL)
			*second_file_name='\0';

		//VALUES logic_operation: AND = 0, OR=1, XOR=2, NOR=3
		char *operation;
		switch(logic_operation){
			case 0:	operation = MALLOC(sizeof(char) * (strlen("_AND_") + strlen(input_files[1]) + 1));
					sprintf(operation, "_AND_%s", input_files[1]);
			break;
			case 1:	operation = MALLOC(sizeof(char) * (strlen("_OR_") + strlen(input_files[1])+ 1));
					sprintf(operation, "_OR_%s", input_files[1]);
			break;
			case 2:	operation = MALLOC(sizeof(char) * (strlen("_XOR_") + strlen(input_files[1]) + 1));
					sprintf(operation, "_XOR_%s", input_files[1]);
			break;
			case 3: operation = MALLOC(sizeof(char) * (strlen("_NOR_") + strlen(input_files[1]) + 1));
					sprintf(operation, "_NOR_%s", input_files[1]);
			break;
		}
		output_filename = create_output_filename(input_files[0], operation, get_extension(format));
		free(operation);

	return output_filename;
}


void initialize_TASK_T(TASK_T *t, NET_PBM *pbm){

	t->matrix = MALLOC(sizeof(NET_PBM));
	*(t->matrix)= *pbm;
	t->matrix->points = (POINT_T **)get_matrix(pbm->lines, pbm->columns, sizeof(POINT_T));
	t->pbm = pbm;

}


void initialize_PARAM_T(PARAM_T *p, int max_threads, int line_blocks){

	p->pos_read = 0;
	p->pos_write = 0;
	p->buffer = MALLOC(sizeof(int)*max_threads);
	p->stop = FALSE;
	p->total = 0;
	p->line_blocks = line_blocks;
	p->max_threads = max_threads;
	pthread_mutex_init(&(p->mutex), NULL);
	pthread_cond_init(&(p->cond), NULL);

}


void **get_matrix(int lines, int cols, size_t type) {
	
	int **m = MALLOC(sizeof(int*) * lines);
	if (!m)
		exit(ERR_ALLOC);

	void *data = MALLOC(type * lines * cols);
	if (!data)
		exit(ERR_ALLOC);

	int i;
	for (i = 0; i < lines; i++)
		m[i] = data + i * cols * type;
	return (void **)m;
}



void *consumer(void *arg){
	PARAM_T *t =  arg;

	int line_index;
	while(1){

		if((errno=pthread_mutex_lock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_LOCK, "pthread_mutex_lock failed!\n");

		//VERIFY IF BUFFER IS EMPTY AND IF PRODUCER HAD NOT REACHED THE END OF THE MATRIX
		while(t->total==0 && t->stop==FALSE)
			if((errno=pthread_cond_wait(&(t->cond), &(t->mutex)))!=0) ERROR(ERR_COND_WAIT, "pthread_cond_wait failed!\n");
		
		//VERIFY IF BUFFER IS EMPTY AND PRODUCER HAD REACHED THE END OF THE MATRIX
		if(t->total==0){
			if((errno=pthread_mutex_unlock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_UNLOCK, "pthread_mutex_unlock failed!\n");
			break;
		}

		line_index=t->buffer[t->pos_read];
		t->pos_read=(t->pos_read+1)%t->max_threads;
		t->total--;

		if(t->total==t->max_threads-1)
			if((errno=pthread_cond_signal(&(t->cond)))!=0) ERROR(ERR_COND_SIGNAL, "pthread_cond_signal failed!\n");

		if((errno=pthread_mutex_unlock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_UNLOCK, "pthread_mutex_unlock failed!\n");

		t->filter(t->filter_arg, line_index, t->line_blocks);

	}

	return NULL;
}

void add_pixel_sobel(void *arg, int line_index, int line_blocks){
	TASK_T *task = arg;

	unsigned short **matrix = (unsigned short **) get_matrix(3, 3, sizeof(unsigned short));
	int columns = task->pbm->columns, lines = task->pbm->lines, i, sum;
	register int j;


	for(i=line_index; i < lines && i < (line_index + line_blocks); i++){
		for(j=0; j < columns; j++){
			//ATRIBUATE ZEROS TO ALL ELEMENTS IN THE MATRIX
			memset(matrix[0], 0, sizeof(unsigned short)*9);

			matrix = add_blocks_sobel(task, matrix, i, j);
		
			//SUM OF THE FIRST MATRIX
						//*-1 			*-2 				*-1 			*1 			*2 					*1
			sum = (-matrix[0][0] - (matrix[0][1]*2) - matrix[0][2] + matrix[2][0] + (matrix[2][1]*2) + matrix[2][2]);

			//SUM OF THE SECOND MATRIX
							//*-1 			*-2 				*-1 		*1 				*2 				*1
			sum += (-matrix[0][0] - (matrix[1][0]*2) - matrix[2][0] + matrix[0][2] + (matrix[1][2]*2) + matrix[2][2]);

			//VERIFY RESULTS
			if(sum<0)
				sum=0;
			else if(sum > task->pbm->max_color)
				sum = task->pbm->max_color;

			task->matrix->points[i][j].rgb[0] = sum;
		}

	}

	free(matrix[0]);
	free(matrix);
}

unsigned short **add_blocks_sobel(TASK_T *task, unsigned short **matrix, int line_index, int column_index){

	int line_begin = 1, column_begin = 1, matrix_line_begin = 0, matrix_column_begin = 0, i, j, k, g;
		
		//VERIFY LINES
		if(line_index<line_begin){
			line_begin = 0;
			matrix_line_begin++;
		}

		//VERIFY COLUMNS
		if(column_index<column_begin){
			column_begin = 0;
			matrix_column_begin++;
		}

			for(k = matrix_line_begin, i = line_index - line_begin; i <= (line_index + 1) && i < task->pbm->lines; k++, i++){
						for(g = matrix_column_begin,j = column_index - column_begin; j <= (column_index + 1) && j < task->pbm->columns; g++, j++){
							
							matrix[k][g] = task->pbm->points[i][j].rgb[0]; 

						}
					}

	return matrix;
}


void convertPPMtoPGM(NET_PBM *pbm){

	int line, column;

		for(line=0; line<pbm->lines;line++){
			for(column=0; column<pbm->columns; column++){
					pbm->points[line][column].rgb[0] = round(pbm->points[line][column].rgb[0]*0.2126+ pbm->points[line][column].rgb[1]*0.7152+ pbm->points[line][column].rgb[2]*0.0722);
			}
		}
		pbm->format = (pbm->format == 3 ? 2 : 5); 
}

				//void * = PARAM_T
void add_pixel_median(void *arg, int line_index, int line_blocks){
	TASK_T *task = arg;

	int i, rgb = 1, n_elements, k;
	register int j;

	//VERIFY FORMAT OF IMAGE
	if(task->pbm->format==3 || task->pbm->format==6)
		rgb=3;
	//ITS +1 BECAUSE I AM GONA SAVE THE LEGHT OF ELEMENTS ON THE LAST INDEX
	unsigned short *vetor = MALLOC(sizeof(unsigned short)*(task->filter_size));
	for(i=line_index; i < (line_index+line_blocks) && i < task->pbm->lines; i++){
		for(j=0; j < task->pbm->columns; j++){
			for(k=0; k<rgb; k++){

				vetor=add_blocks_median(task->pbm, vetor, i, j, k, &n_elements, task->filter_size, task->sqrt_filter_size);				
				qsort(vetor, n_elements, sizeof(unsigned short), comparator);

				if(IS_ODD(n_elements)){
					task->matrix->points[i][j].rgb[k] = vetor[n_elements/2];
				}
				else{
					task->matrix->points[i][j].rgb[k] = AVERAGE(vetor[(n_elements/2)-1], vetor[n_elements/2]);
				}
			}
		}
	}

	free(vetor);
}


unsigned short *add_blocks_median(NET_PBM *pbm, unsigned short *vetor, int line_index, int column_index, int rgb, int *n_elements, int filter_size, int sqrt_filter_size){

	*n_elements = 0;
	int lines = pbm->lines, cols = pbm->columns, line_begin = line_index-sqrt_filter_size, line_end = line_index+sqrt_filter_size, column_begin = column_index-sqrt_filter_size, column_end = column_index+sqrt_filter_size, i, j;

	switch(filter_size){
		case 5: if(line_index>0){
					vetor[*n_elements]=pbm->points[line_index-1][column_index].rgb[rgb];	
					(*n_elements)++;
				}
				
				if(line_index<lines-1){
					vetor[*n_elements]=pbm->points[line_index+1][column_index].rgb[rgb];
					(*n_elements)++;
				}

				if(column_index>0){
					vetor[*n_elements]=pbm->points[line_index][column_index-1].rgb[rgb];
					(*n_elements)++;
				}

				if(column_index<cols-1){
					vetor[*n_elements]=pbm->points[line_index][column_index+1].rgb[rgb];
					(*n_elements)++;
				}

				vetor[*n_elements]=pbm->points[line_index][column_index].rgb[rgb];
				(*n_elements)++;
		break;

		default: 
				//VERIFY LINES
				 if(line_begin<0)
					line_begin=0;
				 else if(line_end>=(pbm->lines-1))
				 		line_end = pbm->lines;

				//VERIFY COLUMNS
				 if(column_begin<0)
				 	column_begin=0;
				 else if(column_end>=(pbm->columns-1))
				 		column_end = pbm->columns;

				 for(i=line_begin; i<line_end; i++){
				 	for(j=column_begin; j<column_end; j++, (*n_elements)++){
				 		vetor[*n_elements]=pbm->points[i][j].rgb[rgb];
				 	}
				 }

		break;
	}
	
	return vetor;
}

void producer(PARAM_T *t, int lines){


	int i;
	for(i=0; i < lines; i+=t->line_blocks){
		if((errno=pthread_mutex_lock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_LOCK, "pthread_mutex_lock failed!\n");

			//VERIFY IF BUFFER HAD REACHED THE MAXIMUM LIMIT
			while(t->total==t->max_threads)
				if((errno=pthread_cond_wait(&(t->cond), &(t->mutex)))!=0) ERROR(ERR_COND_WAIT, "pthread_cond_wait failed\n");
			
			t->buffer[t->pos_write] = i;
			t->pos_write=(t->pos_write+1)%t->max_threads;
			t->total++;

			if(t->total==1)
				if((errno=pthread_cond_broadcast(&(t->cond)))!=0) ERROR(ERR_COND_BROAD, "pthread_cond_broadcast failed!\n");


		if((errno=pthread_mutex_unlock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_UNLOCK, "pthread_mutex_unlock failed!\n");
	}

	//SIGNALIZING THE OTHERS THREADS THAT MATRIX HAD REACHED THE END
	if((errno=pthread_mutex_lock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_LOCK, "pthread_mutex_lock failed!\n");

		t->stop = TRUE;
		if((errno=pthread_cond_broadcast(&(t->cond)))!=0) ERROR(ERR_COND_BROAD, "pthread_cond_broadcast failed!\n");

	if((errno=pthread_mutex_unlock(&(t->mutex)))!=0) ERROR(ERR_MUTEX_UNLOCK, "pthread_mutex_unlock failed!\n");	

}

int comparator (const void * a, const void * b){
		return ( *(int*)a - *(int*)b );
}
